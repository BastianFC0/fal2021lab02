//Bastian Fernandez Cortez
//2041556
public class BikeStore {
    public static void main(String[] args) {
        Bicycle store[] = new Bicycle[4];
        Bicycle bike1 = new Bicycle("MoarBikes", 2, 30.5);
        Bicycle bike2 = new Bicycle("IloveBicycles", 3, 25.3);
        Bicycle bike3 = new Bicycle("SussyBicycles", 2, 35.6);
        Bicycle bike4 = new Bicycle("XpertBiking", 4, 40.0);
        store[0] = bike1;
        store[1] = bike2;
        store[2] = bike3;
        store[3] = bike4;
        for (Bicycle bike : store) {
            System.out.println(bike.toString());
        }
    }
}